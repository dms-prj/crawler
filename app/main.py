import luigi

from crawler import GetCampusDataTask, SendDataToDatabase

if __name__ == "__main__":
    luigi.build([SendDataToDatabase(), GetCampusDataTask()])
