import itertools
import os

import mechanize
from bs4 import BeautifulSoup
from dotenv import load_dotenv
from luigi import LocalTarget, Task
from luigi.contrib import sqla
from luigi.format import UTF8
from sqlalchemy import Integer, String


class GetCampusDataTask(Task):
    """Task for extracting and loading data from Campus Website."""

    def output(self):
        """Returns parsed data from campus site as txt file."""
        return LocalTarget("app/parsed_data.txt".format(), format=UTF8)

    def run(self):
        """Extract and parse data from campus site."""
        load_dotenv()
        url = os.getenv("CAMPUS_URL")
        br = mechanize.Browser()
        # authorize
        br.open(url + os.getenv("CAMPUS_URL_LOGIN"))
        br.select_form(nr=0)
        br.form["LoginForm[username]"] = os.getenv("CAMPUS_USERNAME")
        br.form["LoginForm[password]"] = os.getenv("CAMPUS_PASSWORD")
        br.submit()

        data = []
        count = 1
        while True:
            # get page meta data
            html = BeautifulSoup(br.response().read(), "html.parser")
            table = html.find("table", class_="table table-striped table-bordered")
            table_body = table.find("tbody")

            table_rows = table_body.find_all("tr")
            for table_row in table_rows:
                cols = [el.text.strip() for el in table_row.find_all("td")]
                cols.insert(0, str(count))
                data.append("_".join(cols))
                count += 1

            # get to next page
            pages = html.find("ul", class_="pagination")
            if pages.find("li", class_="next disabled") is not None:
                break
            next_page = pages.find("li", class_="next").find("a")["href"]
            br.open(url + next_page)

        with self.output().open("w") as parsed_data:
            for row in data:
                parsed_data.write(row + "\n")


class SendDataToDatabase(sqla.CopyToTable):
    """Task for loading data from Campus Website to Database."""

    load_dotenv()
    connection_string = (
        "{db_driver}://{db_user}:{db_pass}@{server}:{port}/{db_name}".format(
            db_driver=os.getenv("DB_DRIVER"),
            db_user=os.getenv("DB_USERNAME"),
            db_pass=os.getenv("DB_USER_PASSWORD"),
            server=os.getenv("SERVER"),
            port=os.getenv("PORT"),
            db_name=os.getenv("DB_NAME"),
        )
    )
    table = os.getenv("TABLE_NAME")
    columns = [
        (["id", Integer], {"primary_key": True}),
        (["number", String(10)], {}),
        (["dormitory", String(30)], {}),
        (["capacity", Integer], {}),
        (["freemale", Integer], {}),
        (["freefemale", Integer], {}),
        (["freetotal", Integer], {}),
    ]

    def rows(self):
        """Send rows of data to run() method."""
        with self.input().open("r") as parsed_data:
            lines = parsed_data.readlines()
            for line in lines:
                raw_row = line.split("_")
                yield [
                    raw_row[0],
                    raw_row[2],
                    raw_row[1],
                    raw_row[6],
                    raw_row[4],
                    raw_row[3],
                    raw_row[5],
                ]

    def run(self):
        """Load data to database and clear unnecessary files after work."""
        self._logger.info(
            "Running task copy to table for update id %s for table %s"
            % (self.update_id(), self.table)
        )
        output = self.output()
        engine = output.engine
        self.create_table(engine)
        with engine.begin() as conn:
            conn.execution_options(autocommit=True).execute(
                "TRUNCATE TABLE " + self.table
            )
            rows = iter(self.rows())
            ins_rows = [
                dict(zip(("_" + c.key for c in self.table_bound.c), row))
                for row in itertools.islice(rows, self.chunk_size)
            ]
            while ins_rows:
                self.copy(conn, ins_rows, self.table_bound)
                ins_rows = [
                    dict(zip(("_" + c.key for c in self.table_bound.c), row))
                    for row in itertools.islice(rows, self.chunk_size)
                ]
                self._logger.info(
                    "Finished inserting %d rows into SQLAlchemy target" % len(ins_rows)
                )
        output.touch()

        """Erase unnecessary data from GetCampusDataTask."""
        self.input().remove()

        """Erase output file."""
        with engine.begin() as conn:
            conn.execution_options(autocommit=True).execute(
                "TRUNCATE TABLE table_updates;"
            )

        self._logger.info("Finished inserting rows into SQLAlchemy target")

    def requires(self):
        """Required tasks for this one."""
        return GetCampusDataTask()
