# Backend of Dormitory Management System

To setup project

- Create python3.10 environemnt and **activate it**

```sh
python3.10 -m venv .venv
```

- Install Poetry and Cachecontrol (for Ubuntu)
```sh
sudo apt install python3-poetry
sudo apt install python3-cachecontrol
```

- Install dependencies with poetry

```sh
poetry install
```

- Setup pre-commit

```sh
pre-commit install
```
